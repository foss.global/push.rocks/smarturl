import { expect, tap } from '@pushrocks/tapbundle';
import * as smarturl from '../ts/index.js';

let testSmarturl: smarturl.Smarturl;

tap.test('first test', async () => {
  testSmarturl = new smarturl.Smarturl();
  expect(testSmarturl).toBeInstanceOf(smarturl.Smarturl);
});

tap.test('should parse an URL', async () => {
  const testUrl = 'https://lossless.com:3000/?some=cool&more=yes';
  // const urlMod = await import('url');
  // const altParsed = urlMod.parse(testUrl);
  // console.log(altParsed);
  const parsedUrl = smarturl.Smarturl.createFromUrl(testUrl, {
    searchParams: {
      more: 'overwritten',
    },
  });
  console.log(parsedUrl);
  console.log(parsedUrl.toString());
});

tap.test('should parse an URL', async () => {
  const testUrl = 'https://lossless.com:3000/';
  const parsedUrl = smarturl.Smarturl.createFromUrl(testUrl, {
    searchParams: {
      more: 'overwritten',
    },
  });
  console.log(parsedUrl);
  console.log(parsedUrl.toString());
});

tap.test('should correctly parse ans assemble urls', async () => {
  const testUrl = 'https://lossless.com/';
  const parsedUrl = smarturl.Smarturl.createFromUrl(testUrl, {});
  console.log(parsedUrl.toString());
  expect(parsedUrl.toString()).toEqual('https://lossless.com:443/');
});

tap.start();
