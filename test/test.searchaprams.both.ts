import { expect, tap } from '@pushrocks/tapbundle';
import * as smarturl from '../ts/index.js';

let testSmarturl: smarturl.Smarturl;

tap.test('first test', async () => {
  testSmarturl = new smarturl.Smarturl();
  expect(testSmarturl).toBeInstanceOf(smarturl.Smarturl);
});

tap.test('should create searchParams only', async () => {
  const smarturlInstance = new smarturl.Smarturl();
  smarturlInstance.searchParams['hello'] = 'hi_there';
  console.log(smarturlInstance.toString());
})

tap.start();
